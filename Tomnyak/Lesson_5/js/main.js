function getRandomNumber() {
   return Math.floor((Math.random() * 6) + 1);
}
function printGameStatus(printValue, divId){
    document.getElementById(divId).innerHTML += printValue;
}
function isNumberEqual(firstNum, secondNum) {
    if(firstNum === secondNum) {
        printGameStatus("<div class='result-with-feature'>Выпал дубль. Число " + firstNum + "</div>", "result");
    }
}
function isBigDifference(firstNumber, secondNumber) {
    if( firstNumber < 3 && secondNumber > 4) {
        var numberDifference = secondNumber - firstNumber;
        printGameStatus("<div class='result-with-feature'>Большой разброс между костями. Разница составляет " + numberDifference + "</div>", "result");
    }
}
function gameResult(gameScore){
    return (gameScore > 100) ? "<div class='result-of-game'>Победа, вы набрали " + gameScore + " очков</div>" : "<div class='result-of-game'>Вы проиграли, у вас " + gameScore + " очков</div>";
}
function runDiceGame(){
    var total = 0;
    for(var i = 1; i <= 15; i++){
        if(i == 8 || i == 13) 
		{continue;}
        var first = getRandomNumber();
        var second = getRandomNumber();
        printGameStatus("<div class='result-of-throw'> Первая кость: "+ "<span class='dice'>"+ first +"</span>"+" Вторая кость: " +"<span class='dice'>" + second + "</span>"+ "</div>", "result");
        isNumberEqual (first,second);
        isBigDifference(first,second);
        total += first + second;
    }
    printGameStatus(gameResult(total), "result-of-game");
}
runDiceGame();